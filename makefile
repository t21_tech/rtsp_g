#**************************************************************************\
#                                                                           *
#   Copyright 2006 ViXS Systems Inc.  All Rights Reserved.                  *
#                                                                           *
#===========================================================================*
#                                                                           *
#   File Name: makefile                                                     *
#                                                                           *
#   $Source: /cvs2hg/rsync/XCSDK/XCSDK/XCMSDK/makefile,v $
#                                                                           *
#   $Log: not supported by cvs2svn $
#                                                                           *
#***************************************************************************/

#makefile of XCMSDK

#--------------------------------------------------
#
#   Load configure
#
#--------------------------------------------------

ifeq (.config, $(wildcard .config))
    include .config
endif

include makefile.flag

BUILDROOT_FOLDER=../buildroot/output/staging/usr
BUILDROOT_HOSTFOLDER=../buildroot/output/host/usr

export PATH := $(BUILDROOT_HOSTFOLDER)/bin:$(PATH)
#--------------------------------------------------
#
#   Source file defines
#
#--------------------------------------------------

ifeq "$(TARGET)" "MIPS"
    COMMSDK_DIR=../xcsdk/commsdk/include
    ifeq "$(multicore)" "y"
        DIRECTFB_DIR=../DirectFB/
    else
        DIRECTFB_DIR=../DirectFB/single_core
    endif
    ifeq "$(olddirectfb)" "y"
        DIRECTFB_INC_DIR=../DirectFB/include
    else
        DIRECTFB_INC_DIR=$(DIRECTFB_DIR)/include
    endif
    ifeq "$(dynamiclibrary)" "y"
        DIRECTFB_LIB_DIR=$(DIRECTFB_DIR)/lib/directfb-1.4-0
    else
        ifeq "$(olddirectfb)" "y"
            DIRECTFB_LIB_DIR=$(DIRECTFB_DIR)/lib/directfb-1.4-0
        else
            ifeq "$(newstaticdfbpath)" "y"
                DIRECTFB_LIB_DIR=$(DIRECTFB_DIR)/lib/directfb-1.4-0
            else
                DIRECTFB_LIB_DIR=$(DIRECTFB_DIR)/static_lib/directfb-1.4-0
            endif
        endif
    endif
    ALSA_DIR=../ALSA-mips
    IRDA_DIR=../xcsdk/app/irda/driver
else ifeq "$(TARGET)" "MIPS_IOMEGA"
    COMMSDK_DIR=../xcsdk/commsdk/include
    IRDA_DIR=../xcsdk/app/irda/driver
else ifeq "$(TARGET)" "ARM"
    COMMSDK_DIR=$(BUILDROOT_FOLDER)/include/xcsdk/commsdk/include
    ifeq "$(multicore)" "y"
        DIRECTFB_DIR=$(BUILDROOT_FOLDER)
    else
        DIRECTFB_DIR=../DirectFB/single_core
    endif
    ifeq "$(olddirectfb)" "y"
        DIRECTFB_INC_DIR=../DirectFB/include
    else
        DIRECTFB_INC_DIR=$(DIRECTFB_DIR)/include
    endif
    ifeq "$(dynamiclibrary)" "y"
        DIRECTFB_LIB_DIR=$(DIRECTFB_DIR)/lib/directfb-1.4-0
    else
        ifeq "$(olddirectfb)" "y"
            DIRECTFB_LIB_DIR=$(DIRECTFB_DIR)/lib/directfb-1.4-0
        else
            ifeq "$(newstaticdfbpath)" "y"
                DIRECTFB_LIB_DIR=$(DIRECTFB_DIR)/lib/directfb-1.4-0
            else
                DIRECTFB_LIB_DIR=$(DIRECTFB_DIR)/lib/directfb-1.4-0
            endif
        endif
    endif
    ALSA_DIR=$(BUILDROOT_FOLDER)
    IRDA_DIR=../xcsdk/app/irda/driver
    ifeq "$(supportvfs)" "y"
        VFS_VETH_DIR=../vfs/src
    endif
else
    COMMSDK_DIR=../commsdk/include
    DIRECTFB_DIR=../DirectFB
    DIRECTFB_INC_DIR=$(DIRECTFB_DIR)/include
    DIRECTFB_LIB_DIR=$(DIRECTFB_DIR)/lib/directfb-1.0-0
    ALSA_DIR=../ALSA
endif

ifeq "$(enableffmpeg)" "y"
    FFMPEG_DIR=../ffmpeg
endif

ifeq "$(httpsclient)" "y"
    OPENSSL_DIR=../openssl-1.0.0/include
endif

ifeq "$(httpswebserver)" "y"
    OPENSSL_DIR=../openssl-1.0.0/include
endif

ifeq "$(httpslivestreaming)" "y"
    OPENSSL_DIR=../openssl-1.0.0/include
endif

AUTHOR_DIR=../AuthoringSDK
AUTHOR_SRC_DIR=../AuthoringSDK/source
AUTHOR_LIB_DIR=../AuthoringSDK/lib
VETH_DIR=../veth_vcomm/src
VCOMM_DIR=../veth_vcomm/src
KF_DIR=../KF

LIBMINI_SRC=$(wildcard ./miniserver/src/*.cpp) $(wildcard ./threadutil/src/*.cpp)
LIBRTSP_SRC=$(wildcard src/*.cpp)
LIBWEB_SRC=$(wildcard ./webserver/src/*.cpp)
LIBCDCMSGSERVER_SRC=$(wildcard ../virtual_cdc_msg/server/src/*.cpp) $(wildcard ../virtual_cdc_msg/common/src/*.cpp) libsource/xcarchive_cdc.cpp 
LIBCDCEVTSERVER_SRC=$(wildcard ../virtual_cdc_event/server/src/*.cpp) $(wildcard ../virtual_cdc_event/common/src/*.cpp) libsource/xcarchive_cdc.cpp

LIBVIXS_COMMON_SRC=libsource/autodetector.cpp libsource/pes.cpp libsource/common.cpp libsource/xcodeapi.cpp libsource/xcodeio.cpp libsource/xclog.cpp libsource/xcobject.cpp libsource/xcoutput.cpp libsource/linklist.cpp 
LIBYUV_COMMON_SRC=libsource/font.cpp libsource/yuv_container.cpp libsource/subpic_container.cpp

ifneq "$(disableplayer)" "y"
    LIBPLAYER_COMMON_SRC=libsource/directfboutput.cpp libsource/stc.cpp libsource/hdmipcmremap.cpp libsource/downmixer.cpp libsource/audiomixer.cpp libsource/softlimiter.cpp libsource/pcmencoder.cpp libsource/xcaudrender.cpp \
        libsource/deemph.cpp libsource/pcmresampling.cpp libsource/vbigenerator.cpp
    ifneq "$(disablevcms)" "y"
        LIBPLAYER_COMMON_SRC+=libsource/vcmsav.cpp
    endif
    LIBPLAYER_SRC=libsource/trickmode_source_control.cpp libsource/ts_trickmode_source_control.cpp libsource/ps_trickmode_source_control.cpp libsource/ves_trickmode_source_control.cpp libsource/aes_trickmode_source_control.cpp
endif

LIBVIXS_OPTICAL_DRV_SRC=libsource/optical_drive.cpp libsource/bddrive.cpp libsource/scsicmd.cpp libsource/bd_dvd_mmc.cpp libsource/disk_mgr.cpp libsource/xcode_aacs.cpp libsource/disc_info.cpp


LIBVIXS_BASE_SRC=$(LIBVIXS_COMMON_SRC) $(LIBYUV_COMMON_SRC) $(LIBPLAYER_COMMON_SRC) $(LIBPLAYER_SRC) $(LIBTHUMBNAIL_SRC) $(LIBVIXS_PVR_SRC)\
    libsource/crc.cpp \
    libsource/fileinput.cpp libsource/vfsinput.cpp libsource/fileoutput.cpp \
    libsource/httpinput.cpp libsource/inputxbar.cpp \
    libsource/jpeg2yuv.cpp libsource/m2ts.cpp libsource/psi_si.cpp libsource/mstring.cpp libsource/os.cpp \
    libsource/outputxbar.cpp \
    libsource/ringbuf.cpp libsource/dynamicbuffer.cpp \
    libsource/rtspinput.cpp \
    libsource/textparser.cpp \
    libsource/ts_parser.cpp \
    libsource/xcevent.cpp libsource/xcinput.cpp \
    libsource/xml.cpp libsource/xmlparser.cpp \
    libsource/yuv2jpeg.cpp libsource/yuv.cpp libsource/tsdemuxer.cpp libsource/udpinput.cpp libsource/tcpinput.cpp libsource/rtpinput.cpp libsource/dashinput.cpp libsource/mpegdashbuffer.cpp \
    libsource/rtspservershell.cpp \
    libsource/fileinputwithbuffer.cpp libsource/edid.cpp \
    libsource/genlock.cpp \
    libsource/zixi_input.cpp libsource/spidev_test.cpp libsource/HTTPDownloader.cpp libsource/rtmpinput.cpp \
    libsource/cap708_bufreader.cpp libsource/ccwindow.cpp

ifeq "$(enablercsp)" "y"
	LIBVIXS_BASE_SRC += libsource/rcsp_input.cpp
endif

ifneq "$(nointerpreter)" "y"
    LIBVIXS_BASE_SRC += libsource/eventinterpreter.cpp libsource/inputinterpreter.cpp libsource/inputxbarinterpreter.cpp \
        libsource/osinterpreter.cpp libsource/outputinterpreter.cpp libsource/outputxbarinterpreter.cpp libsource/systeminterpreter.cpp \
        libsource/networkinterpreter.cpp 
endif

ifneq "$(noreceptionist)" "y"
    LIBVIXS_BASE_SRC += libsource/serialreceptionist.cpp libsource/netreceptionist.cpp libsource/webreceptionist.cpp libsource/multiplereceptionistadapter.cpp
endif

LIBVIXS_BASE_SRC+= libsource/charset.cpp 

ifneq "$(disableavisupport)" "y"
    LIBVIXS_BASE_SRC+= libsource/riff.cpp libsource/avi.cpp libsource/avidemux.cpp
endif
ifneq "$(disableisomsupport)" "y"
    LIBVIXS_BASE_SRC+= libsource/isom.cpp libsource/isomdemux.cpp
endif
ifneq "$(disablemkvsupport)" "y"
    LIBVIXS_BASE_SRC+= libsource/ebml.cpp libsource/matroska.cpp libsource/mkvdemux.cpp
endif
ifneq "$(disableflvsupport)" "y"
    LIBVIXS_BASE_SRC+= libsource/flv.cpp libsource/flvdemux.cpp
endif
ifneq "$(disableasfsupport)" "y"
    LIBVIXS_BASE_SRC+= libsource/asf.cpp
endif

ifeq (libsource/version.cpp, $(wildcard libsource/version.cpp))
    LIBVIXS_BASE_SRC += libsource/version.cpp
endif

ifeq "$(enableffmpeg)" "y"
    LIBVIXS_BASE_SRC+=libsource/ffmpegapi.cpp
endif

LIBVIXS_AUTHORING_SRC=$(AUTHOR_SRC_DIR)/author.cpp $(AUTHOR_SRC_DIR)/authoringctrl.cpp $(AUTHOR_SRC_DIR)/authorcommon.cpp \
    $(AUTHOR_SRC_DIR)/bd_author.cpp $(AUTHOR_SRC_DIR)/bdav_author.cpp $(AUTHOR_SRC_DIR)/bd_remux.cpp $(AUTHOR_SRC_DIR)/bd_ts2m2ts.cpp $(AUTHOR_SRC_DIR)/bd_disk.cpp \
    $(AUTHOR_SRC_DIR)/dvd_author.cpp $(AUTHOR_SRC_DIR)/dvd_ifo.cpp $(AUTHOR_SRC_DIR)/dvd_ps2vob.cpp $(AUTHOR_SRC_DIR)/dvd_remux.cpp $(AUTHOR_SRC_DIR)/dvd_vob.cpp $(AUTHOR_SRC_DIR)/dvd_vobu_file.cpp $(AUTHOR_SRC_DIR)/dvd_disk.cpp \
    $(AUTHOR_SRC_DIR)/dvdvr_vob_parser.cpp $(AUTHOR_SRC_DIR)/dvdvr_author.cpp $(AUTHOR_SRC_DIR)/dvdvr_disk.cpp $(AUTHOR_SRC_DIR)/data_author.cpp\
    $(AUTHOR_SRC_DIR)/hddvd_author.cpp $(AUTHOR_SRC_DIR)/hddvd_evob.cpp $(AUTHOR_SRC_DIR)/hddvd_evobu_file.cpp $(AUTHOR_SRC_DIR)/hddvd_hli.cpp $(AUTHOR_SRC_DIR)/hddvd_ifo.cpp $(AUTHOR_SRC_DIR)/hddvd_ps2evob.cpp $(AUTHOR_SRC_DIR)/hddvd_remux.cpp


SOURCE_SRC_FILE=$(wildcard ./source/*.cpp)
SOURCE_SRC_FILE+=$(wildcard ./peripheral/*.cpp)
SOURCE_SRC_FILE+=$(wildcard ./peripheral/MxLWare/*.cpp)
SOURCE_SRC_FILE+=$(wildcard ./peripheral/TDA19971/*.cpp)
SOURCE_INC_FILE=$(wildcard ./source/*.h) 
SOURCE_INC_FILE+=$(wildcard ./peripheral/inc/*.h) 
SOURCE_OBJ_FILE=$(patsubst %.cpp, %.o,$(SOURCE_SRC_FILE))
SRC_FILE=$(wildcard ./*.cpp)


#--------------------------------------------------
#
#   Include file defines
#
#--------------------------------------------------
INC_FILE=$(wildcard ./*.h)
OBJ_FILE=$(patsubst %.cpp, %.o,$(SRC_FILE))
INCLUDE_FILES=$(wildcard ./include/*.h) $(wildcard ./source/*.h) $(wildcard ./peripheral/inc/*.h) $(wildcard ./miniserver/inc/*.h) $(wildcard ./threadutil/inc/*.h) $(wildcard ./webserver/inc/*.h) $(wildcard ./rtsp/inc/*.h)

LIBAUTHOR_INCLUDE=-I$(AUTHOR_DIR)/include
LIBMINI_INCLUDE=-I./miniserver/inc -I./threadutil/inc -I./include
LIBWEB_INCLUDE=-I./webserver/inc -I./miniserver/inc -I./threadutil/inc -I./include
LIBRTSP_INCLUDE=-Iinc 
#-I./webserver/inc -I./miniserver/inc -I./threadutil/inc -I./include -I./libsource/inc
LIBVOSD_INCLUDE=-I./vosd/inc -I$(DIRECTFB_INC_DIR)/directfb -I./include  -I$(COMMSDK_DIR) -I$(DIRECTFB_INC_DIR) 
LIBVOSDPLUS_INCLUDE=-I./vosdplus/inc -I$(DIRECTFB_INC_DIR)/directfb -I./include -I$(DIRECTFB_INC_DIR)/ 
LIBVIXS_INCLUDE=-I./include -I./libsource/inc -I./miniserver/inc -I./threadutil/inc -I./webserver/inc -I./rtsp/inc -I$(DIRECTFB_INC_DIR)/directfb -I$(BUILDROOT_FOLDER)/include/dispsdk/inc -I$(ALSA_DIR)/include/alsa -I$(COMMSDK_DIR)

ifeq "$(cdcserver)" "y"
    LIBVIXS_INCLUDE+= $(LIBCDCSERVER_INCLUDE)
endif

LIBVIXS_INCLUDE+= -I$(IRDA_DIR)

ifeq "$(httpsclient)" "y"
    LIBVIXS_INCLUDE+= -I$(OPENSSL_DIR)
endif

ifeq "$(httpswebserver)" "y"
    LIBVIXS_INCLUDE+= -I$(OPENSSL_DIR)
endif

ifeq "$(httpslivestreaming)" "y"
    LIBVIXS_INCLUDE+= -I$(OPENSSL_DIR)
endif

LIBVFS_INCLUDE=-I./include -I./libsource/inc -I./vfs/inc -I$(VETH_DIR) -I$(COMMSDK_DIR)
INCLUDE=-I./include -I./miniserver/inc -I./threadutil/inc -I./webserver/inc -I./rtsp/inc -I./vosd/inc -I./peripheral/inc -I$(DIRECTFB_INC_DIR)/directfb -I$(ALSA_DIR)/include/ -I$(ALSA_DIR)/include/alsa -I$(COMMSDK_DIR)

LIBVIXS_INCLUDE+= $(LIBVFS_INCLUDE)

ifneq "$(disableplayer)" "y"
LIBVIXS_INCLUDE+= -I$(DIRECTFB_INC_DIR)
#LIBVIXS_BASE_SRC+=libsource/JPEGdecoder.cpp 
endif

ifeq "$(dtcp)" "y"
LIBVIXS_INCLUDE+= -I../DLNA/dtcp/inc
endif

ifeq "$(cdcserver)" "y"
    INCLUDE+= $(LIBCDCSERVER_INCLUDE)
endif

LIBMINI_INC_FILE=$(wildcard ./miniserver/inc/*.h) $(wildcard ./threadutil/inc/*.h) $(wildcard ./include/*.h)
LIBWEB_INC_FILE=$(wildcard ./webserver/inc/*.h) $(wildcard ./miniserver/inc/*.h) $(wildcard ./threadutil/inc/*.h) $(wildcard ./include/*.h)
LIBRTSP_INC_FILE=$(wildcard inc/*.h) 
#$(wildcard ./webserver/inc/*.h) $(wildcard ./miniserver/inc/*.h) $(wildcard ./threadutil/inc/*.h) $(wildcard ./include/*.h)
LIBVOSD_INC_FILE=$(wildcard ./vosd/inc/*.h) $(wildcard ./include/*.h)
LIBVOSDPLUS_INC_FILE=$(wildcard ./vosdplus/inc/*.h) $(wildcard ./include/*.h)
LIBVIXS_INC_FILE=$(wildcard ./include/*.h) $(wildcard ./libsource/inc/*.h) $(wildcard ./miniserver/inc/*.h) $(wildcard ./threadutil/inc/*.h) $(wildcard ./webserver/inc/*.h) $(wildcard ./rtsp/inc/*.h) $(wildcard ../virtual_cdc_msg/server/inc/*.h) $(wildcard ../virtual_cdc_msg/common/inc/*.h) $(wildcard ../virtual_cdc_event/server/inc/*.h) $(wildcard ../virtual_cdc_event/common/inc/*.h)
LIBAUTHOR_INC_FILE=$(wildcard ./include/*.h) $(wildcard $(AUTHOR_DIR)/include/*.h) $(wildcard $(AUTHOR_SRC_DIR)/inc/*.h)
LIBCAPI_INC_FILE=$(wildcard ./include/*.h) $(wildcard ./capi/include/*.h)
LIBVFS_INC_FILE=$(wildcard ./include/*.h) $(wildcard ./vfs/inc/*.h)

#--------------------------------------------------
#
#   Library file defines
#
#--------------------------------------------------
LIBDIR=./lib
LIBVOSDDIR=./lib
LIBVOSDPLUSDIR=./vosdplus/lib
LIBCAPIDIR=./capi/lib
LIBVFSDIR=./lib
LIBCDCMSGSERVERDIR=../virtual_cdc_msg/server/lib
LIBCDCEVTSERVERDIR=../virtual_cdc_event/server/lib
LIBPLAYERCONTROLLERDIR = ../bd/playersController/lib
LIBAUTHDRIVE = ../KF/lib

ifeq "$(TARGET)" "ARC"
    LIBPLATFORMDIR=./lib/arc
endif

ifeq "$(TARGET)" "ARC-GNU"
    LIBPLATFORMDIR=./lib/arc-gnu
endif

ifeq "$(TARGET)" "MIPS"
    LIBPLATFORMDIR=./lib/mips
endif

ifeq "$(TARGET)" "MIPS_IOMEGA"
    LIBPLATFORMDIR=./lib/mips
endif

ifeq "$(TARGET)" "ARM"
    LIBPLATFORMDIR=$(BUILDROOT_FOLDER)/lib/arm
endif

ifneq "$(disableplayer)" "y"
    ifeq "$(dynamiclibrary)" "y"
    	ifeq "$(TARGET)" "ARM"
            # Following modules are dynamically loaded by DirectFB itself from path defined in /etc/directfbrc (default is /usr/vixs/lib/directfb-1.4-0)
            # -L$(DIRECTFB_LIB_DIR)/systems -ldirectfb_devmem -L$(DIRECTFB_LIB_DIR)/gfxdrivers/ -ldirectfb_vixs -L$(DIRECTFB_LIB_DIR)/wm -ldirectfbwm_default -L$(DIRECTFB_LIB_DIR)/interfaces/IDirectFBFont -lidirectfbfont_ft2 -L$(DIRECTFB_LIB_DIR)/interfaces/IDirectFBImageProvider -lidirectfbimageprovider_png -lidirectfbimageprovider_jpeg -lidirectfbimageprovider_gif -lidirectfbimageprovider_bmp
            DIRECTFB_LIBS=-L$(DIRECTFB_DIR)/lib -ldirectfb -ldirect -lpng -ljpeg -lfusion -lfreetype -lz
        else
            DIRECTFB_LIBS=-L$(DIRECTFB_LIB_DIR)/systems -ldirectfb_devmem -L$(DIRECTFB_LIB_DIR)/gfxdrivers/ -ldirectfb_vixs -L$(DIRECTFB_LIB_DIR)/wm -ldirectfbwm_default -L$(DIRECTFB_LIB_DIR)/interfaces/IDirectFBFont -lidirectfbfont_ft2 -L$(DIRECTFB_LIB_DIR)/interfaces/IDirectFBImageProvider -lidirectfbimageprovider_png -lidirectfbimageprovider_jpeg -lidirectfbimageprovider_gif -lidirectfbimageprovider_bmp -L$(DIRECTFB_DIR)/lib -ldirectfb -ldirect -lpng -ljpeg -lfusion -lfreetype -lz
        endif
    else
        DIRECTFB_LIBS=$(DIRECTFB_LIB_DIR)/systems/libdirectfb_devmem.o $(DIRECTFB_LIB_DIR)/gfxdrivers/libdirectfb_vixs.o $(DIRECTFB_LIB_DIR)/wm/libdirectfbwm_default.o $(DIRECTFB_LIB_DIR)/interfaces/IDirectFBFont/libidirectfbfont_ft2.o $(DIRECTFB_LIB_DIR)/interfaces/IDirectFBImageProvider/libidirectfbimageprovider_png.o -L$(DIRECTFB_DIR)/lib -ldirectfb -ldirect -lpng -ljpeg -lfusion -lfreetype -lz
    endif

    ALSA_LIB=-L$(ALSA_DIR)/lib -lasound
else
    # If DirectFB is not available, use internal jpeg lib
    DIRECTFB_LIBS=-ljpeg
endif

LIBS=-L$(LIBDIR) -L$(LIBPLATFORMDIR) -lvixs $(DIRECTFB_LIBS) $(ALSA_LIB)

ifeq "$(authoring)" "y"
    LIBS+=-L$(AUTHOR_LIB_DIR) -lauthor
    ifeq "$(enablebdmvsupport)" "y"
        #LIBS+= -lmpeg2 -lmpeg2convert -lvo
    endif
    ifeq "$(enabledvdvideosupport)" "y"
        #LIBS+= -lmpeg2 -lmpeg2convert -lvo
    endif
    ifeq "$(enablehddvdvideosupport)" "y"
        #LIBS+= -lmpeg2 -lmpeg2convert -lvo
    endif
endif

ifneq "$(disablewebserver)" "y"
    LIBS+=-lweb
endif

ifneq "$(disableminiserver)" "y"
    LIBS+=-lmini
endif

ifneq "$(disablertspsupport)" "y"
    LIBS+=-lrtsp
endif

ifneq "$(disableplayer)" "y"
    # Fix a bug of ARC linker, place -ljpeg twice, before/after the -lvixs
    LIBS+=-ljpeg
endif

ifneq "$(disableplayer)" "y"
    ifeq "$(viper)" "y"
        LIBS+=-ldtsneo6 -lmlpdrc
    endif
endif

# Make sure all standard libs are linked last, to workaround the compiler library order issue
ifeq "$(TARGET)" "MIPS"
    LIBS+= -lrt -lm -ldl -lstdc++ -Wl,-Bdynamic -lpthread
else ifeq "$(TARGET)" "MIPS_IOMEGA"
    LIBS+= -lrt -lm -ldl -lstdc++ -Wl,-Bdynamic -lpthread
else ifeq "$(TARGET)" "ARM"
    LIBS+= -lrt -lm -ldl -lstdc++ -Wl,-Bdynamic -lpthread
else
    LIBS+= -lrt -lm -ldl -lstdc++ -lpthread
endif

# Shared libs link here
ifneq "$(disablevcms)" "y"
    LIBS+=-lvcmsdref
endif

ifeq "$(cdcserver)" "y"
    LIBS+= -lrt -lcrypto
endif

ifeq "$(dynamiclibrary)" "y"
    ifeq "$(disableplayer)" "y"
        LIB_CAPI=
    else
        LIB_CAPI=$(LIBCAPIDIR)/libcapi.so
    endif

    LIB_VFS=$(LIBVFSDIR)/libvfs.so

    ifeq "$(vfsveth)" "y"
        LIBS+=-lvfs -lvixs
    endif

    ifeq "$(supportvfs)" "y"
        LIBS+=-lm $(VFS_VETH_DIR)/vfs_veth.a
    endif

    ifeq "$(vfsusb)" "y"
        LIBS+=-lvfs -lvixs
    endif

    ifneq "$(disableminiserver)" "y"
        LIB_MINI=$(LIBDIR)/libmini.so
    endif
    ifneq "$(disablewebserver)" "y"
        LIB_WEB=$(LIBDIR)/libweb.so
    endif
    ifneq "$(disablertspsupport)" "y"
        LIB_RTSP=librtsp.so
    endif

    LIB_VOSD=$(LIBDIR)/libvosd.so
    LIB_VOSDPLUS=$(LIBVOSDPLUSDIR)/libvosdplus.so
    LIB_VIXS=$(LIBDIR)/libvixs.so
    LIB_AUTHOR=$(AUTHOR_LIB_DIR)/libauthor.so

else
    ifeq "$(disableplayer)" "y"
        LIB_CAPI=
    else
        LIB_CAPI=$(LIBCAPIDIR)/libcapi.a
    endif
    LIB_VFS=$(LIBVFSDIR)/libvfs.a

    ifeq "$(vfsveth)" "y"
        LIBS+=-lvfs -lvixs
    endif

    ifeq "$(supportvfs)" "y"
        LIBS+=-lm $(VFS_VETH_DIR)/vfs_veth.a
    endif

    ifeq "$(vfsusb)" "y"
        LIBS+=-lvfs -lvixs
    endif

    ifneq "$(disableminiserver)" "y"
        LIB_MINI=$(LIBDIR)/libmini.a
    endif
    ifneq "$(disablewebserver)" "y"
        LIB_WEB=$(LIBDIR)/libweb.a
    endif
    ifneq "$(disablertspsupport)" "y"
        LIB_RTSP=$(LIBDIR)/librtsp.a
    endif

    ifeq "$(viper)" "y"
        ifneq "$(stripcustomers)" "y"
            LIB_DVB=$(LIBDIR)/libdvb.a
        endif
    endif
        
    LIB_VOSD=$(LIBVOSDDIR)/libvosd.a
    LIB_VOSDPLUS=$(LIBVOSDPLUSDIR)/libvosdplus.a
    LIB_VIXS=$(LIBDIR)/libvixs.a
    LIB_AUTHOR=$(AUTHOR_LIB_DIR)/libauthor.a
    LIB_CDCMSGSERVER=$(LIBCDCMSGSERVERDIR)/libcdcmsgserver.a
    LIB_CDCEVTSERVER=$(LIBCDCEVTSERVERDIR)/libcdcevtserver.a
endif

ifeq "$(enableffmpeg)" "y"
    #LIBS+=-L$(FFMPEG_DIR) -L$(FFMPEG_DIR)/libavcodec -L$(FFMPEG_DIR)/libavfilter -L$(FFMPEG_DIR)/libavformat -L$(FFMPEG_DIR)/libavdevice -L$(FFMPEG_DIR)/libavutil -L$(FFMPEG_DIR)/libpostproc -L$(FFMPEG_DIR)/libswscale
    ## MAKE SURE FFmpeg ARE LINKED DYNAMICALLY, SINCE IT'S UNDER LGPL or GPL LICENSE!!!
    LIBS+=-Wl,-Bdynamic -lavformat -lavcodec -lavfilter -lavdevice -lswscale -lavutil -lm
    ifeq "$(ffmpeg10)" "y"
        LIBS+=-lswresample
    endif
    ifeq "$(ffmpeg11)" "y"
        LIBS+=-lswresample
    endif
endif

LIB_FILE_ALL=$(LIB_VIXS) $(LIB_AUTHOR) $(LIB_WEB) $(LIB_MINI) $(LIB_RTSP) $(LIB_VOSD)

ifeq "$(viper)" "y"
    ifneq "$(stripcustomers)" "y"
        LIB_FILE_ALL += $(LIB_DVB)
    endif
endif

ifeq "$(cdcserver)" "y"
    LIB_FILE_ALL += $(LIB_CDCMSGSERVER)
    LIB_FILE_ALL += $(LIB_CDCEVTSERVER)
endif

#SDK build
ifeq "$(buildlib)" "y"
    LIB_FILE=$(LIB_VIXS) $(LIB_WEB) $(LIB_MINI) $(LIB_RTSP) 
    
    ifeq "$(viper)" "y"
        ifneq "$(stripcustomers)" "y"
            LIB_FILE += $(LIB_DVB)
        endif
    endif

    ifeq "$(authoring)" "y"
        LIB_FILE+= $(LIB_AUTHOR)
    endif

    ifeq "$(vfsveth)" "y"
        LIB_FILE+= $(LIB_VFS)
    endif
    ifeq "$(vfsusb)" "y"
        LIB_FILE+= $(LIB_VFS)
    endif

    ifeq "$(cdcserver)" "y"
        LIB_FILE+= $(LIB_CDCMSGSERVER)
        LIB_FILE+= $(LIB_CDCEVTSERVER)
    endif
else    # buildlib != y
    LIB_FILE=
endif   # buildlib

ifeq "$(vosdplus)" "y"
    LIB_FILE+= $(LIB_VOSDPLUS)
endif


#--------------------------------------------------
#
#   Compile option defines
#
#--------------------------------------------------
ifeq "$(dynamiclibrary)" "y"
    ifeq "$(TARGET)" "X86"
        CXX=g++ -g
        LD=g++
        AR=g++ -shared -lc -o
    else
        ifeq "$(TARGET)" "ARC-GNU"
            CXX=arc-linux-gnu-gcc -g -DTARGETARC -mA7 -Wno-invalid-offsetof
            LD=arc-linux-gnu-gcc -g  -mA7
            AR=arc-linux-gnu-gcc -shared -lc -o
        else
            ifeq "$(TARGET)" "MIPS"
                CXX=mipsisa32r2el-timesys-linux-gnu-gcc -Wl,-Bdynamic -g -DTARGETMIPS -mlong-calls -Wno-invalid-offsetof -fno-strict-aliasing
                LD=mipsisa32r2el-timesys-linux-gnu-gcc -Wl,-Bdynamic -g -mlong-calls
                AR=mipsisa32r2el-timesys-linux-gnu-gcc -shared -lc -o
            else ifeq "$(TARGET)" "MIPS_IOMEGA"
                CXX=mipsel-unknown-linux-gnu-gcc -Wl,-Bdynamic -g -DTARGETMIPS -mlong-calls -Wno-invalid-offsetof
                LD=mipsel-unknown-linux-gnu-gcc -Wl,-Bdynamic -g -mlong-calls
                AR=mipsel-unknown-linux-gnu-gcc -shared -lc -o
            else ifeq "$(TARGET)" "ARM"
                CXX=arm-linux-gnueabihf-gcc -Wl,-Bdynamic -g -DTARGETARM -mlong-calls -Wno-invalid-offsetof -fno-strict-aliasing -Wno-psabi
                LD=arm-linux-gnueabihf-gcc -Wl,-Bdynamic -g -mlong-calls
                AR=arm-linux-gnueabihf-gcc -shared -lc -o
            else
                CXX=arc-linux-uclibc-gcc -g -DTARGETARC -mA7 -Wno-invalid-offsetof
                LD=arc-linux-uclibc-gcc -g -mA7
                AR=arc-linux-uclibc-gcc -shared -lc -o
            endif
        endif
    endif
    DYNAMIC_CFLAGS=-fPIC
else
    ifeq "$(TARGET)" "X86"
        CXX=g++ -g
        LD=g++ -g
        STRIP=strip -d
        AR=ar rcs 
    else
        ifeq "$(TARGET)" "ARC-GNU"
            CXX=arc-linux-gnu-gcc -g -DTARGETARC -mlong-calls -mA7 -Wno-invalid-offsetof
            LD=arc-linux-gnu-gcc -g -mlong-calls -mA7
            STRIP=arc-linux-gnu-strip -d
            AR=arc-linux-gnu-ar rcs 
        else
            ifeq "$(TARGET)" "MIPS"
                CXX=mipsisa32r2el-timesys-linux-gnu-gcc -Wl,-Bstatic -g -DTARGETMIPS -mlong-calls -Wno-invalid-offsetof -fno-strict-aliasing
                LD=mipsisa32r2el-timesys-linux-gnu-gcc -Wl,-Bstatic -g -mlong-calls
                STRIP=mipsisa32r2el-timesys-linux-gnu-strip -d
                AR=mipsisa32r2el-timesys-linux-gnu-ar rcs
            else ifeq "$(TARGET)" "MIPS_IOMEGA"
                CXX=mipsel-unknown-linux-gnu-gcc -Wl,-Bstatic -g -DTARGETMIPS -mlong-calls -Wno-invalid-offsetof
                LD=mipsel-unknown-linux-gnu-gcc -Wl,-Bstatic -g -mlong-calls
                STRIP=mipsel-unknown-linux-gnu-strip -d
                AR=mipsel-unknown-linux-gnu-ar rcs
            else ifeq "$(TARGET)" "ARM"
                CXX=arm-linux-gnueabihf-gcc -Wl,-Bstatic -g -DTARGETARM -mlong-calls -Wno-invalid-offsetof -fno-strict-aliasing -Wno-psabi
                LD=arm-linux-gnueabihf-gcc -Wl,-Bstatic -g -mlong-calls
                STRIP=arm-linux-gnueabihf-strip -d
                AR=arm-linux-gnueabihf-ar rcs
            else
                CXX=arc-linux-uclibc-gcc -static -g -DTARGETARC -mlong-calls -mA7 -Wno-invalid-offsetof
                LD=arc-linux-uclibc-gcc  -g -mlong-calls -mA7
                STRIP=arc-linux-uclibc-strip -d
                AR=arc-linux-uclibc-ar rcs 
            endif
        endif
    endif
    DYNAMIC_CFLAGS=
endif

LIBMINI_CC_OPTS=$(LIBMINI_INCLUDE) -c -DWEBSERVER_ONLY $(XCHWCONFIG_CFLAGS) $(XCCHIPSET_CFLAGS) $(EXTRA_CFLAGS) $(DYNAMIC_CFLAGS)
LIBWEB_CC_OPTS=$(LIBWEB_INCLUDE) -c -DWEBSERVER_ONLY $(XCHWCONFIG_CFLAGS) $(XCCHIPSET_CFLAGS) $(EXTRA_CFLAGS) $(DYNAMIC_CFLAGS)
LIBRTSP_CC_OPTS=$(LIBRTSP_INCLUDE) -c -Wno-deprecated -DSOCKLEN_T=socklen_t -D_LARGEFILE_SOURCE=1 -DBSD -Dlinux_gnu -DLINUX $(XCHWCONFIG_CFLAGS) $(XCCHIPSET_CFLAGS) $(EXTRA_CFLAGS) $(DYNAMIC_CFLAGS) 
LIBCAPI_CC_OPTS=$(LIBCAPI_INCLUDE) -c -fomit-frame-pointer -Dlinux_gnu -DLINUX -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE $(XCHWCONFIG_CFLAGS) $(XCCHIPSET_CFLAGS) $(EXTRA_CFLAGS) $(DYNAMIC_CFLAGS)
LIBVFS_CC_OPTS=$(LIBVFS_INCLUDE) -c -fomit-frame-pointer -Dlinux_gnu -DLINUX -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE $(XCHWCONFIG_CFLAGS) $(XCCHIPSET_CFLAGS) $(EXTRA_CFLAGS) $(DYNAMIC_CFLAGS)

ifeq "$(viper)" "y"
    LIBDVB_CC_OPTS=$(LIBDVB_INCLUDE) -c -Wno-deprecated -DSOCKLEN_T=socklen_t -D_LARGEFILE_SOURCE=1 -DBSD -Dlinux_gnu -DLINUX $(XCHWCONFIG_CFLAGS) $(XCCHIPSET_CFLAGS) $(EXTRA_CFLAGS) $(DYNAMIC_CFLAGS)
endif


ifeq "$(TARGET)" "X86"
    LIBVOSD_CC_OPTS=$(LIBVOSD_INCLUDE) -Wall -pipe $(XCHWCONFIG_CFLAGS) $(XCCHIPSET_CFLAGS) $(EXTRA_CFLAGS) $(DYNAMIC_CFLAGS)
else
    LIBVOSD_CC_OPTS=$(LIBVOSD_INCLUDE) -Dlinux_gnu -DLINUX -Wall -pipe -DARC $(XCHWCONFIG_CFLAGS) $(XCCHIPSET_CFLAGS) $(EXTRA_CFLAGS) $(DYNAMIC_CFLAGS)
endif

ifeq "$(TARGET)" "X86"
    LIBVOSDPLUS_CC_OPTS=$(LIBVOSDPLUS_INCLUDE) -Wall -pipe $(XCHWCONFIG_CFLAGS) $(XCCHIPSET_CFLAGS) $(EXTRA_CFLAGS) $(DYNAMIC_CFLAGS)
else
    LIBVOSDPLUS_CC_OPTS=$(LIBVOSDPLUS_INCLUDE) -Dlinux_gnu -DLINUX -Wall -pipe -DARC $(XCHWCONFIG_CFLAGS) $(XCCHIPSET_CFLAGS) $(EXTRA_CFLAGS) $(DYNAMIC_CFLAGS)
endif

LIBVIXS_CC_OPTS=$(LIBVIXS_INCLUDE) -c -fomit-frame-pointer -Dlinux_gnu -DLINUX -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE $(XCHWCONFIG_CFLAGS) $(XCCHIPSET_CFLAGS) $(EXTRA_CFLAGS) $(DYNAMIC_CFLAGS)

ifeq "$(authoring)" "y"
    LIBVIXS_CC_OPTS += $(LIBAUTHOR_INCLUDE) -I$(KF_DIR)/inc
    LIBAUTHOR_CC_OPTS=$(LIBVIXS_CC_OPTS) -I$(AUTHOR_SRC_DIR)/inc -I$(KF_DIR)/inc
endif

ifeq "$(enableffmpeg)" "y"
    # From FFmpeg 1.0, time.h is added to FFmpeg source code and make result link to that time.h instead of the toolchain time.h, DO NOT include $(FFMPEG_DIR)/libavutil path
    #LIBVIXS_CC_OPTS+=-I$(FFMPEG_DIR) -I$(FFMPEG_DIR)/libavcodec -I$(FFMPEG_DIR)/libavfilter -I$(FFMPEG_DIR)/libavformat -I$(FFMPEG_DIR)/libavdevice -I$(FFMPEG_DIR)/libavutil -I$(FFMPEG_DIR)/libpostproc -I$(FFMPEG_DIR)/libswscale
    LIBVIXS_CC_OPTS+=-I$(BUILDROOT_FOLDER)/include -Wno-deprecated-declarations
endif

CC_OPTS=$(INCLUDE) -c -fomit-frame-pointer -Dlinux_gnu -DLINUX -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE $(XCHWCONFIG_CFLAGS) $(XCCHIPSET_CFLAGS) $(EXTRA_CFLAGS) $(DYNAMIC_CFLAGS)
ifeq "$(authoring)" "y"
    CC_OPTS += $(LIBAUTHOR_INCLUDE)
endif
LINK_OPTS=-Dlinux_gnu -DLINUX -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE $(XCHWCONFIG_CFLAGS) $(XCCHIPSET_CFLAGS) $(EXTRA_CFLAGS) $(DYNAMIC_CFLAGS)


#--------------------------------------------------
#
#   Object file defines
#
#--------------------------------------------------
LIBMINI_OBJ=$(patsubst %.cpp, %.o,$(LIBMINI_SRC))
LIBRTSP_OBJ=$(patsubst %.cpp, %.o,$(LIBRTSP_SRC))
ifeq "$(viper)" "y"
    LIBDVB_OBJ=$(patsubst %.cpp, %.o,$(LIBDVB_SRC))
endif
LIBVOSD_OBJ=$(patsubst %.cpp, %.o,$(LIBVOSD_SRC))
LIBVOSDPLUS_OBJ=$(patsubst %.cpp, %.o,$(LIBVOSDPLUS_SRC))
LIBWEB_OBJ=$(patsubst %.cpp, %.o,$(LIBWEB_SRC))
LIBVIXS_OBJ=$(patsubst %.cpp,%.o,$(LIBVIXS_BASE_SRC))
LIBAUTHOR_OBJ=$(patsubst %.cpp,%.o,$(LIBVIXS_AUTHORING_SRC))
LIBCAPI_OBJ=$(patsubst %.cpp,%.o,$(LIBCAPI_SRC))
LIBVFS_OBJ=$(patsubst %.cpp,%.o,$(LIBVFS_SRC))
LIBCDCMSGSERVER_OBJ=$(patsubst %.cpp, %.o,$(LIBCDCMSGSERVER_SRC))
LIBCDCEVTSERVER_OBJ=$(patsubst %.cpp, %.o,$(LIBCDCEVTSERVER_SRC))
LIB_OBJ_FILE=$(LIBMINI_OBJ) $(LIBRTSP_OBJ) $(LIBWEB_OBJ) $(LIBVIXS_OBJ) $(LIBAUTHOR_OBJ) $(LIBVOSD_OBJ) $(LIBCAPI_OBJ) $(LIBVFS_OBJ) $(LIBVQOS_OBJ) $(LIBCDCMSGSERVER_OBJ) $(LIBCDCEVTSERVER_OBJ)


OUT_FILE=./test

OUT=-o $(OUT_FILE)


#--------------------------------------------------
#
#   Make target defines
#
#--------------------------------------------------
ifeq "$(stripcustomers)" "y"
all: $(LIB_FILE) $(SOURCE_OBJ_FILE) $(OBJ_FILE)
	@echo "  LD    "$(OUT_FILE)
	$(LD) $(LINK_OPTS) $(SOURCE_OBJ_FILE) $(OBJ_FILE) $(LIBS) $(OUT)
else
all: $(LIB_FILE) $(SOURCE_OBJ_FILE) $(OBJ_FILE)
	@echo "  LD    "$(OUT_FILE)
	@$(LD) $(LINK_OPTS) $(SOURCE_OBJ_FILE) $(OBJ_FILE) $(LIBS) $(OUT)
ifneq "$(dynamiclibrary)" "y"
	@echo "  STRIP "$(OUT_FILE)
	@$(STRIP) $(OUT_FILE)
endif
endif

libs: $(LIB_FILE)


$(SOURCE_OBJ_FILE): %.o : %.cpp $(SOURCE_INC_FILE) $(INCLUDE_FILES) 
	@echo "  CXX   "$<
	@$(CXX) $(CC_OPTS) -o $@ $<

$(OBJ_FILE): %.o : %.cpp $(INC_FILE) $(INCLUDE_FILES)
	@echo "  CXX   "$<
	@$(CXX) $(CC_OPTS) -o $@ $<

libmini: $(LIB_MINI)
$(LIB_MINI): $(LIBMINI_OBJ)
	@echo "  AR    "$(LIB_MINI)
	@$(AR) $(LIB_MINI) $(LIBMINI_OBJ)

$(LIBMINI_OBJ): %.o : %.cpp $(LIBMINI_INC_FILE)
	@echo "  CXX   "$<
	@$(CXX) $(LIBMINI_CC_OPTS) -o $@ $<

libweb: $(LIB_WEB)
$(LIB_WEB): $(LIBWEB_OBJ)
	@echo "  AR    "$(LIB_WEB)
	@$(AR) $(LIB_WEB) $(LIBWEB_OBJ)

$(LIBWEB_OBJ): %.o : %.cpp $(LIBWEB_INC_FILE)
	@echo "  CXX   "$<
	@$(CXX) $(LIBWEB_CC_OPTS) -o $@ $<

librtsp: $(LIB_RTSP)
$(LIB_RTSP): $(LIBRTSP_OBJ)
	@echo "  AR    "$(LIB_RTSP)
	@$(AR) $(LIB_RTSP) $(LIBRTSP_OBJ)

$(LIBRTSP_OBJ): %.o : %.cpp $(LIBRTSP_INC_FILE)
	#@echo "  CXX   "$<
	$(CXX) $(LIBRTSP_CC_OPTS) $(LIBRTSP_BR_OPTS) -o $@ $<
	
ifeq "$(viper)" "y" 
libdvb: $(LIB_DVB)
$(LIB_DVB): $(LIBDVB_OBJ)
	@echo "  AR    "$(LIB_DVB)
	@$(AR) $(LIB_DVB) $(LIBDVB_OBJ)

$(LIBDVB_OBJ): %.o : %.cpp $(LIBDVB_INC_FILE)
	@echo "  CXX   "$<
	@$(CXX) $(LIBDVB_CC_OPTS) -o $@ $<
endif

libvosd: $(LIB_VOSD)
$(LIB_VOSD): $(LIBVOSD_OBJ)
	 @if [ ! -d $(LIBVOSDDIR) ]; \
        then \
        mkdir $(LIBVOSDDIR) ; \
        fi
	@echo "  AR    "$(LIB_VOSD)
	@$(AR) $(LIB_VOSD) $(LIBVOSD_OBJ) 

$(LIBVOSD_OBJ): %.o : %.cpp $(LIBVOSD_INC_FILE)
	@echo "  CXX   "$<
	@$(CXX)  $(LIBVOSD_CC_OPTS) -o $@ -c $<

libvosdplus: $(LIB_VOSDPLUS)
$(LIB_VOSDPLUS): $(LIBVOSDPLUS_OBJ)
	 @if [ ! -d $(LIBVOSDPLUSDIR) ]; \
        then \
        mkdir $(LIBVOSDPLUSDIR) ; \
        fi
	@echo "  AR    "$(LIB_VOSDPLUS)
	@$(AR) $(LIB_VOSDPLUS) $(LIBVOSDPLUS_OBJ) 

$(LIBVOSDPLUS_OBJ): %.o : %.cpp $(LIBVOSDPLUS_INC_FILE)
	@echo "  CXX   "$<
	@$(CXX)  $(LIBVOSDPLUS_CC_OPTS) -o $@ -c $<

libvixs: $(LIB_VIXS)
$(LIB_VIXS): $(LIBVIXS_OBJ)
	@echo "  AR    "$(LIB_VIXS)
	@$(AR) $(LIB_VIXS) $(LIBVIXS_OBJ)

$(LIBVIXS_OBJ): %.o : %.cpp $(LIBVIXS_INC_FILE)
	@echo "  CXX   "$<
	@$(CXX) -g $(LIBVIXS_CC_OPTS) -o $@ $<

libauthor: $(LIB_AUTHOR)
$(LIB_AUTHOR): $(LIBAUTHOR_OBJ)
	@echo "  AR    "$(LIB_AUTHOR)
	@$(AR) $(LIB_AUTHOR) $(LIBAUTHOR_OBJ)

$(LIBAUTHOR_OBJ): %.o : %.cpp $(LIBAUTHOR_INC_FILE)
	@echo "  CXX   "$<
	@$(CXX) $(LIBAUTHOR_CC_OPTS) -o $@ $<

libcapi: $(LIB_CAPI)
$(LIB_CAPI): $(LIBCAPI_OBJ)
	@if [ ! -d $(LIBCAPIDIR) ]; \
        then \
        mkdir $(LIBCAPIDIR) ; \
        fi
	@echo "  AR    "$(LIB_CAPI)
	@$(AR) $(LIB_CAPI) $(LIBCAPI_OBJ)

$(LIBCAPI_OBJ): %.o : %.cpp $(LIBCAPI_INC_FILE)
	@echo "  CXX   "$<
	@$(CXX) $(LIBCAPI_CC_OPTS) -o $@ $<

libvfs: $(LIB_VFS)
$(LIB_VFS): $(LIBVFS_OBJ)
	@echo "  AR    "$(LIB_VFS)
	@$(AR) $(LIB_VFS) $(LIBVFS_OBJ)

$(LIBVFS_OBJ): %.o : %.cpp $(LIBVFS_INC_FILE)
	@echo "  CXX   "$<
	@$(CXX) $(LIBVFS_CC_OPTS) -o $@ $<

ifeq "$(cdcserver)" "y"
libcdcmsgserver: $(LIB_CDCMSGSERVER) 
$(LIB_CDCMSGSERVER): $(LIBCDCMSGSERVER_OBJ)
	@echo "  AR    "$(LIB_CDCMSGSERVER)
	@$(AR) $(LIB_CDCMSGSERVER) $(LIBCDCMSGSERVER_OBJ)

libcdcevtserver: $(LIB_CDCEVTSERVER) 
$(LIB_CDCEVTSERVER): $(LIBCDCEVTSERVER_OBJ)
	@echo "  AR    "$(LIB_CDCEVTSERVER)
	@$(AR) $(LIB_CDCEVTSERVER) $(LIBCDCEVTSERVER_OBJ)	
endif

#--------------------------------------------------
#
#   Clean target defines
#
#--------------------------------------------------
cleanall:
	rm -f $(OUT_FILE)
	rm -f $(OBJ_FILE)
	rm -f $(SOURCE_OBJ_FILE)
	@echo "Clean capi"; rm -f $(LIB_CAPI) $(LIBCAPI_OBJ)
	@if [ -d miniserver/src ]; then echo "Clean miniserver"; rm -f $(LIBMINI_OBJ) $(LIB_MINI) ; fi
	@if [ -d webserver/src ]; then echo "Clean webserver"; rm -f $(LIBWEB_OBJ) $(LIB_WEB) ; fi
	@if [ -d rtsp/src ]; then echo "Clean rtsp"; rm -f $(LIBRTSP_OBJ) $(LIB_RTSP) ; fi	
	@if [ -d dvb/src ]; then echo "Clean dvb"; rm -f $(LIBDVB_OBJ) $(LIB_DVB) ; fi
	@if [ -d libsource ]; then echo "Clean libsource"; rm -f $(LIBVIXS_OBJ) $(LIBAUTHOR_OBJ) $(LIB_VIXS) $(LIB_AUTHOR) libsource/*.o ; fi
	@if [ -d vosd/src ]; then echo "Clean vosd"; rm -f $(LIBVOSD_OBJ) $(LIB_VOSD) ; fi
	@if [ -d vosdplus/src ]; then echo "Clean vosdplus"; rm -f $(LIBVOSDPLUS_OBJ) $(LIB_VOSDPLUS) ; fi
	@if [ -d vfs/src ]; then echo "Clean vfs"; rm -f $(LIBVFS_OBJ) $(LIB_VFS) ; fi
	@if [ -d ../virtual_cdc_msg/server/src ] && [ -d ../virtual_cdc_msg/common/src ] && [ -d ../virtual_cdc_msg/server/lib ]; then echo "Clean cdcmsgserver"; rm -f $(LIBCDCMSGSERVER_OBJ) $(LIB_CDCMSGSERVER) ; fi
	@if [ -d ../virtual_cdc_event/server/src ] && [ -d ../virtual_cdc_event/common/src ] && [ -d ../virtual_cdc_event/server/lib ]; then echo "Clean cdceventserver"; rm -f $(LIBCDCEVTSERVER_OBJ) $(LIB_CDCEVTSERVER) ; fi

clean:
	rm -f $(OUT_FILE)
	rm -f $(OBJ_FILE)
	rm -f $(SOURCE_OBJ_FILE)
	rm -f $(LIB_CAPI) $(LIBCAPI_OBJ)

cleanlibmini:
	rm -f $(LIB_MINI)
	rm -f $(LIBMINI_OBJ)

cleanlibrtsp:
	rm -f $(LIB_RTSP)
	rm -f $(LIBRTSP_OBJ)

cleanlibdvb:
	rm -f $(LIB_DVB)
	rm -f $(LIBDVB_OBJ)

cleanlibvosd:
	rm -f $(LIB_VOSD)
	rm -f $(LIBVOSD_OBJ)
	
cleanlibvosdplus:
	rm -f $(LIB_VOSDPLUS)
	rm -f $(LIBVOSDPLUS_OBJ)

cleanlibweb:
	rm -f $(LIB_WEB)
	rm -f $(LIBWEB_OBJ)

cleanlibvixs:
	rm -f $(LIB_VIXS)
	rm -f $(LIBVIXS_OBJ) libsource/*.o

cleanlibauthor:
	rm -f $(LIB_AUTHOR)
	rm -f $(LIBAUTHOR_OBJ)

cleanlibcapi:
	rm -f $(LIB_CAPI)
	rm -f $(LIBCAPI_OBJ)

cleanlibvfs:
	rm -f $(LIB_VFS)
	rm -f $(LIBVFS_OBJ)

cleancdcserver:
	rm -f $(LIB_CDCMSGSERVER)
	rm -f $(LIBCDCMSGSERVER_OBJ)
	rm -f $(LIB_CDCEVTSERVER)
	rm -f $(LIBCDCEVTSERVER_OBJ)

#--------------------------------------------------
#
#	Configure
#
#--------------------------------------------------
configure:
	@chmod +x Configure
	./Configure config.in

cleanconfig:
	@rm .config -f > /dev/null

libvixs-install: libvixs
	install -D -m 755 $(LIB_VIXS) $(DESTDIR)/usr/lib/libvixs.so
	
librtsp-install: librtsp
	install -D -m 755 $(LIB_RTSP) $(DESTDIR)/usr/lib/librtsp.so
